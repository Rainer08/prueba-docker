## Solución Pregunta N2

### Pasos 

1. Escribir el Dockerfile, se baja la última imagen de nginx del registry, se añade el archivo index.html al directorio del webserver del container /usr/share/nginx/html/
2. Se crea un volumen que será atachado a otro container de imagen Debian con nombre docker-nginx-files, este container se utilizará de storage para cualquier cambio en el archivo index.html sin que el container web con nombre docker-nginx tenga que ser eliminado y vuelto a correr.
3. Para editar el volumen atachado al container Debian se debe correr el comando 3 y hacer los cambios requeridos en el index.html en la ruta /usr/share/nginx/html, previamente actualizado el container debian y la instalación de vim

### Comandos

1. `docker build . -t nginx`
2. `docker run -it --name docker-nginx -p 8080:80 -d nginx`
3. `docker run -it --volumes-from docker-nginx --name docker-nginx-files debian`
4. `apt-get update -y && apt-get install vim -y`
5. `vim /usr/share/nginx/html/index.html`
6. `docker attach docker-nginx-files`