## Solución Pregunta N1

### Pasos usando script de deploy

1. Escribir el Dockerfile, se baja la última imagen de nginx del registry, se añade el archivo index.html al directorio del webserver del container /usr/share/nginx/html/
2. Escribir script para hacer build de la imagen de nginx, el run con el mapeo de puertos del container de manera desatachado
3. `bash deploy.sh`

### Pasos usando comandos docker

1. `docker build . -t nginx`
2. `docker run --name container_nginx -p 8080:80 -d nginx`

