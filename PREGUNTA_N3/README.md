## Solución Pregunta 3

### Comandos

1. Bajar la imagen MySQL `docker pull mysql/mysql-server:latest` 
2. Correr el container de MySQL-SERVER `docker run --name=container_mysql_server -d mysql/mysql-server:latest`
3. Bajar cliente MySQL `apt-get install mysql-client`
4. Buscar la contraseña ROOT en los logs del MySQL-server `docker logs 61d646a73784 | grep PASSWORD` 
5. Conectarse al container de la base de datos `docker exec -it container_mysql_server bash`
6. Conectarse a MySQl `mysql -u root -p`
7. Cambiar el password por defecto de la base de datos `ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';`
8. Crear nuevo usuario `CREATE USER 'user1'@'localhost' IDENTIFIED BY 'password';`
9. Se le entregan privilegios de root `GRANT ALL PRIVILEGES ON *.* TO user1@localhost;`
10. Verificar privilegios de root `SHOW GRANTS FOR user1@localhost;`
11. Salir y verificar conexión `mysql -u user1 -p`